**DOCTRINE-MANAGER**
==============================

Simple doctrine manager for repositories.
Only support for XMLMapping.

EntityRepository is completely isolated to avoid coupling to doctrine.


**Usage**
==============================
- Create an abstract class that extends from 
  Insidesuki\DoctrineManager\AbstractDoctrineRepository:


    use Insidesuki\DoctrineManager\AbstractDoctrineRepository;

    abstract class ExampleRepository extends AbstractDoctrineRepository
    {

        /**
        * @param string $entity  entity to manage
        * @throws ORMException
        * Uses a settings php array
        */
        public function __construct(string $entity)
        {
            $settings = require __DIR__ . '/../config/db_settings.php';
            parent::__construct($entity, $settings['db_test']);
        }

        /**
        * XML mapping
        * @return string
        */
        protected static function mapping(): string
        {
            return __DIR__ . '/../Mappings';
        }
    }

- Each entity repository must extend from ExampleRepository
- By overloading the constructor, the entity that will manage the repository is specified.

       class EstadoDoctrineRepository extends ExampleRepository 
       {

        public function __construct()
        {
           parent::__construct(Entity::class);
        }


        public function findById(int $id): ?Entity
        {
           return $this->find($id);
        }

       /**
        * @throws \Doctrine\ORM\OptimisticLockException
        * @throws \Doctrine\ORM\ORMException
        */
        public function store(Entity $entity): Entity
         {
           $this->save($entity);
           return $entity;
         }
      }
**DB Settings example**
==============================
- Array php

      <?php
        // /config/db_settings
      return [
              'db_test' => [
                'driver' => 'pdo_mysql',
                'host' => 'any_host',
                'port' => '3306',
                'dbname' => 'db_name',
                'user' => 'db_user',
                'password' => 'db_pass'
            ]
      ];

- Or use .env files, whatever you like (*Recommended) 

