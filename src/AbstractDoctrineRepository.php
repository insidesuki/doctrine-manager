<?php

namespace Insidesuki\DoctrineManager;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

abstract class AbstractDoctrineRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    protected $entityRepository;

    /**
     * @param string $entity
     * @param array $params
     * @throws ORMException
     */
    public function __construct(string $entity, array $params)
    {

        $entityManagerFactory = new EntityManagerFactory($params, $this->mapping());
        $this->em = $entityManagerFactory->build();

        $this->entityRepository = new EntityRepository($this->em, new ClassMetadata($entity));


    }

    /**
     * @return array|object[]
     */
    protected function findAll(): array
    {

        return $this->entityRepository->findAll();
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param $limit
     * @param $offset
     * @return array
     */
    protected function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->entityRepository->findBy($criteria, $orderBy, $limit, $orderBy);
    }

    /**
     * Find one by criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @return mixed|object|null
     */
    protected function findOneBy(array $criteria, ?array $orderBy = null)
    {
        return $this->entityRepository->findOneBy($criteria, $orderBy);
    }

    /**
     * @param $id
     * @return mixed|object|null
     */
    protected function find($id)
    {
        return $this->entityRepository->find($id);
    }


    abstract protected static function mapping();

    /**
     * @throws ORMException
     */
    protected function delete($entity)
    {
        $this->em->remove($entity);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    protected function save($entity, bool $flush = true)
    {

        $this->em->persist($entity);
        ($flush) ? $this->em->flush() : '';

    }

    
    public function getEntityManager(){
        return $this->em;
    }


}
