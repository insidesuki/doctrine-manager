<?php

namespace Insidesuki\DoctrineManager;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Insidesuki\DoctrineManager\Exceptions\DoctrineManagerException;

class EntityManagerFactory
{


    public $isDevMode = true;
    public $proxyDir = null;
    public $cache = null;

    private $config;
    private $mappingPath;

    /**
     * @var array of db params
     */
    private $params = [];

    public function __construct(array $params, string $mappingPath)
    {

        $this->params = $params;
        $this->setMapping($mappingPath);
        $this->config();

    }

    private function setMapping(string $mappingPath)
    {
        if (!is_dir($mappingPath)) {
            throw new DoctrineManagerException(sprintf('The mapping path "%s" does not exists!!', $mappingPath));
        }

        $this->mappingPath = realpath($mappingPath);

    }

    private function config()
    {
        $this->config = Setup::createXMLMetadataConfiguration(array($this->mappingPath),
            $this->isDevMode, $this->proxyDir, $this->cache);
    }

    /**
     * @throws ORMException
     */
    public function build(): EntityManager
    {

        return EntityManager::create($this->params,$this->config);
    }


}